## Dockerized FTP Server

### Usage

Start the docker container by running:

    docker-compose up --build ftp

Start a bash prompt inside the running container:

    docker-compose exec ftp /bin/bash

Add an FTP user:

    pure-pw useradd jco -f /etc/pure-ftpd/passwd/pureftpd.passwd -m -u ftpuser -d /home/ftpusers/jco

Connect to the FTP server from the host:

    ftp -p localhost 21
